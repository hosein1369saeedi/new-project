<!doctype html>
<html lang="en">
    <?php include("blocks/head.php"); ?>
    <body>
        <?php include("blocks/menu-2.php"); ?>
        <?php include("blocks/slider.php"); ?>
        <?php include("blocks/product.php"); ?>
        <?php include("blocks/about-us.php"); ?>
        <?php include("blocks/project-2.php"); ?>
        <?php include("blocks/news.php"); ?>
        <?php include("blocks/brands.php"); ?>
        <?php include("blocks/footer.php"); ?>
        <?php include("blocks/script.php"); ?>
    </body>
</html>