<!doctype html>
<html lang="en">
<?php include("blocks/head.php"); ?>
<body>
<?php include("blocks/menu-2.php"); ?>
<?php include("blocks/slider2.php"); ?>
<div class="prodetails">
<div class="titles container " style="clear: both;">
    <br>
    <h3 align="center" class="mt10"> Elevator moution control system</h3>
</div>
<br>
<br>
<div class="container" style="padding:0px">
<div class="content">
    <div class="col-md-6">

    </div>
    <div class="col-md-6">

    </div>
</div>

<div class="tab">

    <section id="tabs" class="">
        <div class="container" style="padding:0px">
            <div class="row">
                <div class="col-md-12">
                    <nav>
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">General Feature</a>
                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Functions</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
<br>
<br>
                            <section class="features_table">
                                <div class="row ">
                                    <div class="col-sm-2 col-8 col-xs-12 no-padding">
                                        <div class="features-table">
                                            <ul>
                                                <h1>Features</h1>
                                                <li>Elevator Type</li>
                                                <li>User Interface</li>
                                                <li>Rated Power</li>
                                                <li>Motor Control</li>
                                                <li>Maximum Elevator Speed</li>
                                                <li>Number of stops</li>
                                                <li>Group Working Feature</li>
                                                <li>Serial Communication</li>
                                                <li>Encoder</li>
                                                <li>Traffic Control</li>
                                                <li>Communication Interface</li>
                                                <li>Standard Conformity</li>

                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-2 col-xs-12 no-padding">
                                        <div class="features-table-free">
                                            <ul>
                                                <h1>Gen1</h1>
                                                <li>Electric Traction</li>
                                                <li>On-board LCD and Keypad</li>
                                                <li>7.5 KW</li>
                                                <li>PMSM (Sync. Gearless Motor)
                                                    IM (Async. Geared Motor)</li>
                                                <li>1.6m/s</li>
                                                <li>8 Stops</li>
                                                <li>&nbsp;</li>
                                                <li>CANBus Serial Communication</li>
                                                <li>Incremental Encoder (5V TTL, 10~30V HTL, SIN/COS)
                                                    Absolute Encoder (EnDat, SSI, BISS)</li>
                                                <li>
                                                        Selective-Collective
                                                    ,Full-Collective
                                                    ,Up-Collective
                                                   , Down-Collective</li>

                                            </ul>
                                        </div>

                                    </div>
                                    <div class="col-sm-5 col-2 col-xs-12 no-padding">
                                        <div class="features-table-paid">
                                            <ul>
                                                <h1>Gen2</h1>
                                                <li>Electric Traction,
                                                    Hydraulic</li>
                                                <li>On-board 7 segment,
                                                    Handheld Configuration Terminal</li>
                                                <li>7.5 KW,
                                                    11 KW,
                                                    15 KW</li>
                                                <li>PMSM (Sync. Gearless Motor),
                                                    IM (Async. Geared Motor)</li>
                                                <li>4m/s</li>
                                                <li>32 Stops</li>
                                                <li>Upto 8 elevators</li>
                                                <li>2 * CANBus Serial Communication for car and shaft operations,
                                                    CANOpen-Lift</li>
                                                <li>Incremental Encoder (5V TTL, 10~30V HTL, SIN/COS),
                                                    Absolute Encoder (EnDat, SSI, BISS)</li>
                                                <li>Selective-Collective,
                                                    Full-Collective,
                                                    Up-Collective,
                                                    Down-Collective</li>
                                                <li>USB Comm. for configurator software,
                                                    GPRS/GPS Comm. for online monitoring and control</li>
                                                <li>EN 81-20,
                                                    EN 12015, EN 12016</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </section>


                        </div>
                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


</div>
<hr>
<style>
    .prodetails .titles h3{width:50%;padding:15px 0px}
    .project-tab {
        padding: 10%;
        margin-top: -8%;
    }
    .project-tab #tabs{
        background: #007b5e;
        color: #eee;
    }
    .project-tab #tabs h6.section-title{
        color: #eee;
    }
    .project-tab #tabs .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
        color: #0062cc;
        background-color: transparent;
        border-color: transparent transparent #f3f3f3;
        border-bottom: 3px solid !important;
        font-size: 16px;
        font-weight: bold;
    }
    .project-tab .nav-link {
        border: 1px solid transparent;
        border-top-left-radius: .25rem;
        border-top-right-radius: .25rem;
        color: #0062cc;
        font-size: 16px;
        font-weight: 600;
    }
    .project-tab .nav-link:hover {
        border: none;
    }
    .project-tab thead{
        background: #f3f3f3;
        color: #333;
    }
    .project-tab a{
        text-decoration: none;
        color: #333;
        font-weight: 600;
    }



    .features_table h1{
        font-size:25px !important;

    }
    li{
        list-style:none;
    }
    h1{
        font-size:21px !important;
    }
    .features-table li {
        background: #409780 none repeat scroll 0 0;
        border-bottom: 2px solid #56b39a;
        color: #f1f1f1;
        font-size: 16px;
        padding: 15px 24px;
        height:70px
    }
    .features-table li:hover{
        background:  #e5a253;
        cursor:pointer;
        -webkit-transition: all .35s;
        -moz-transition: all .35s;
        transition: all .35s;
    }
    .features-table-free li, .features-table-paid li {
        background: #efefef none repeat scroll 0 0;
        border-bottom: 2px solid #d4d4d4;
        text-align:center;
        padding: 16.4px 21px;
        cursor:pointer;
        height: 70px;
    }
    .features-table-free h1, .features-table-paid h1 {
        text-align: center;
    }
    .features-table-free li:hover, .features-table-paid li:hover {
        background: #dedede none repeat scroll 0 0;
        -webkit-transition: all .35s;
        -moz-transition: all .35s;
        transition: all .35s;
    }
    .features_table h1 {
        font-size: 23px !important;
    }
    .features-table h1, .features-table-free h1, .features-table-paid h1 {
        background: #6b6b6b none repeat scroll 0 0;
        color: #fff;
        font-weight: 600;
        margin: 0;
        padding: 19px 21px;
        text-transform: uppercase;
    }
    .features-table h1 {
        border-top-left-radius: 20px;
        text-align:center;
    }
    .features-table-paid h1 {
        border-top-right-radius: 20px;
    }
    .features-table-free li {
        border-right: 2px solid #d4d4d4;
    }

    .fa.fa-check {
        color: #2cc14f;
    }
    .fa.fa-times {
        color: #BA5340;
    }
    .fa{
        font-size:30px;
    }
    .no-padding{
        padding:0;
    }
    ul{
        padding:0;
    }

    body {
        counter-reset: section;                   /* Set the section counter to 0 */
    }
    .features-table li::before {
        counter-increment: section;               /* Increment the section counter*/
        content: "" counter(section) ": "; /* Display the counter */
    }
</style>
</div>
<?php include("blocks/footer.php"); ?>
<?php include("blocks/script.php"); ?>
</body>
</html>