<!doctype html>
<html lang="en">
<?php include("blocks/head.php"); ?>
<body>
<?php include("blocks/menu-2.php"); ?>
<?php include("blocks/slider2.php"); ?>

<div class="titles container" style="clear: both;">
<br>
    <h3 align="center" class="mt10"> contact </h3>
</div>
<div class="container contact">
    <br>
    <div class="row information">
        <div class="col-md-6 left">
            <h4 class="text-center">branch one</h4>
            <hr>
            <div class="row">
                <div class="col-md-4 text-center">
                    <div class="text">
                        <img src="images/placeholder-filled-point.png" alt="">
                    </div>
                    <hr>
                    <a href=""><p>location-location-location</p></a>
                </div>
                <div class="col-md-4 text-center">
                    <div class="text">
                        <img src="images/old-typical-phone.png" alt="">
                    </div>
                    <hr>
                    <a href=""><p>001-12345678</p></a>
                    <a href=""><p>001-12345678</p></a>
                </div>
                <div class="col-md-4 text-center">
                    <div class="text">
                        <img src="images/envelope.png" alt="">
                    </div>
                    <hr>
                    <a href=""><p>info@email.com</p></a>
                    <a href=""><p>info@email.com</p></a>
                </div>
            </div>
            <br>
            <div>
                <h5>location-1</h5>
                <hr>
                <div id="contact" class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3149.9195705182588!2d51.42086638796755!3d35.70003600813093!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1541621317440" width="1400" height="600" frameborder="0" style="border:0" allowfullscreen>    </iframe>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <h4 class="text-center">branch two</h4>
            <hr>
            <div class="row">
                <div class="col-md-4 text-center">
                    <div class="text">
                        <img src="images/placeholder-filled-point.png" alt="">
                    </div>
                    <hr>
                    <a href=""><p>location-location-location</p></a>
                </div>
                <div class="col-md-4 text-center">
                    <div class="text">
                        <img src="images/old-typical-phone.png" alt="">
                    </div>
                    <hr>
                    <a href=""><p>001-12345678</p></a>
                    <a href=""><p>001-12345678</p></a>
                </div>
                <div class="col-md-4 text-center">
                    <div class="text">
                        <img src="images/envelope.png" alt="">
                    </div>
                    <hr>
                    <a href=""><p>info@email.com</p></a>
                    <a href=""><p>info@email.com</p></a>
                </div>
            </div>
            <br>
            <div>
                <h5>location-2</h5>
                <hr>
                <div id="contact" class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3149.9195705182588!2d51.42086638796755!3d35.70003600813093!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1541621317440" width="1400" height="600" frameborder="0" style="border:0" allowfullscreen>    </iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="row form">
        <div class="contact7">
            <div class="container">
                <div class="text-center">
                    <h1>contact us</h1>
                    <hr>
                </div>
                <div class="col-md-12 colxs-12 col-sm-12">
                    <div class="contact7-form">
                        <!-- Form -->
                        <form role="form" action="#">
                            <div class="form-group">
                                <textarea class="form-control message" rows="10" placeholder="your message" required></textarea>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Name" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="email" required>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="phone" required>
                            </div>
                            <button type="submit" class="btn btn-default">send message</button>
                        </form>
                        <!-- /Form -->
                    </div>
                </div>
            </div>
            <!-- <div class="contact7-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <h2>get in touch</h2>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam nibh. Nunc varius facilisis eros. Sed erat. In in velit quis arcu ornare laoreet. Curabitur adipiscing luctus massa.</p>
                        </div>
                        <div class="col-md-4">
                            <h2>contact Details</h2>
                            <p><i class="fa fa-home fa-lg"></i> 123 St. building rex, abc road, new york city, USA</p>
                            <p><i class="fa fa-lg fa-phone"></i> (00) 123 456 7890</p>
                            <p><i class="fa fa-lg fa-fax"></i> 00) 123 456 7890</p>
                            <p><i class="fa fa-lg fa-globe"></i><a href="#"> www.xyz.com</a></p>
                            <p><i class="fa fa-lg fa-envelope-o"></i><a href="#"> support@abcd.com</a></p>
                        </div>
                        <div class="col-md-4">
                            <h2>where to find us</h2>
                            <div>
                                <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.uk/maps?f=q&source=s_q&hl=en&geocode=&q=15+Springfield+Way,+Hythe,+CT21+5SH&aq=t&sll=52.8382,-2.327815&sspn=8.047465,13.666992&ie=UTF8&hq=&hnear=15+Springfield+Way,+Hythe+CT21+5SH,+United+Kingdom&t=m&z=14&ll=51.077429,1.121722&output=embed"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>
<hr>

<?php include("blocks/footer.php"); ?>
<?php include("blocks/script.php"); ?>
</body>
</html>