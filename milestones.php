<!doctype html>
<html lang="en">
<?php include("blocks/head.php"); ?>
<body>
<?php include("blocks/menu-2.php"); ?>
<?php include("blocks/slider2.php"); ?>

<div class="titles container" style="clear: both;">
<br>
    <h3 align="center" class="mt10"> Milestones </h3>
</div>
<br>
<br>
<div class=" about container">

    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="main-timeline5">
                <div class="timeline">
                    <div class="timeline-icon"><span class="year">2019</span></div>
                    <div class="timeline-content">
                       <p class="title"> * 	Moving HQ to Shenzhen, China</p>
                        <p class="title">*  	EMCS1000 (Final) prototype to acquire Int'l standards (EN 81-20, EN 81-50, EN 12015, EN 12016, IEC 61508, ISO 22201, IEC 61800)</p>
                        <p class="title">*  	EMCS1000 (Final) prototype to acquire Int'l standards (EN 81-20, EN 81-50, EN 12015, EN 12016, IEC 61508, ISO 22201, IEC 61800)</p>
                        <p class="title">*  	EMCS1000 (Final) prototype to acquire Int'l standards (EN 81-20, EN 81-50, EN 12015, EN 12016, IEC 61508, ISO 22201, IEC 61800)</p>
                        <p class="title">* 	Expansion of Elevator Portfolio</p>


                    </div>
                </div>
                <div class="timeline">
                    <div class="timeline-icon"><span class="year">2018</span></div>
                    <div class="timeline-content">
                        <p class="title"> * 	EMCS1000 (Beta) prototype successfully passed Field tests</p>
                        <p class="title"> * 	EMCS1000 (Final) prototype designed to meet global market demands</p>
                        <p class="title"> * 	Selected by Iranian Ministry of Energy (as Project Sponsor) to prepare Iranian Elevator & Escalator Standards based on the latest Int'l standards</p>
                        <p class="title"> * 	Major Expansion of R&D Division</p>

                    </div>
                </div>
                <div class="timeline">
                    <div class="timeline-icon"><span class="year">2017</span></div>
                    <div class="timeline-content">
                        <p class="title"> * 		EMCS1000 (Alpha) prototype tested at Lab</p>
                        <p class="title"> * 		EMCS1000 (Beta) prototype designed and tested successfully at Lab</p>

                    </div>
                </div>
                <div class="timeline">
                    <div class="timeline-icon"><span class="year">2016</span></div>
                    <div class="timeline-content">
                        <p class="title"> *	Elevator R&D project (EMCS1000) initiated in Apr. 2016</p>
                        <p class="title"> *	EMCS1000 (Alpha) version designed</p>
                        <p class="title"> *	R&D Lab established</p>

                    </div>
                </div>
                <div class="timeline">
                    <div class="timeline-icon"><span class="year">2015</span></div>
                    <div class="timeline-content">
                        <p class="title">	Ramin Amiri found the company in June 2015 at Tehran, Iran</p>
                        <p class="title">	Marketing for a wide range of motor drive applications carried out.</p>
                        <p class="title">	Comprehensive 5-Year Business Plan drafted as to design and manufacture "Elevator Control Systems" for global market</p>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<style>
    .timeline .title{font-weight:normal;font-size:14px}
    .main-timeline5 .timeline-content{
        overflow-y: scroll;
        height:165px;
        overflow-y: auto;
    }
</style>

<?php include("blocks/footer.php"); ?>
<?php include("blocks/script.php"); ?>
</body>
</html>