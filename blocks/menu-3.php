<div class="menu-3">
<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top top-nav">
    <a class="navbar-brand" href="#">
        <img src="images/logo-parstronic.png" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        <div class="dropdown">
            <button class="dropbtn">Dropdown</button>
            <div class="dropdown-content">
                <a href="#">Link 1</a>
                <a href="#">Link 2</a>
                <a href="#">Link 3</a>
            </div>
        </div> 
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Category 1
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
    
            
            <div class="container">
                <div class="row">
                    
                </div>
            </div>
            <!--  /.container  -->
    
    
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Category 2
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
    
    
            <div class="container">
                <div class="row">
                <div class="col-md-4">
                    <span class="text-uppercase text-white">Category 2</span>
                    <ul class="nav flex-column">
                    <li class="nav-item">
                    <a class="nav-link active" href="#">Active</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#">Link item</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#">Link item</a>
                    </li>
                </ul>
                </div>
                <!-- /.col-md-4  -->
                <div class="col-md-4">
                    <ul class="nav flex-column">
                    <li class="nav-item">
                    <a class="nav-link active" href="#">Active</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#">Link item</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#">Link item</a>
                    </li>
                </ul>
                </div>
                <!-- /.col-md-4  -->
                <div class="col-md-4">
                    <a href="">
                    <img src="https://dummyimage.com/200x100/ccc/000&text=image+link" alt="" class="img-fluid">
                    </a>
                    <p class="text-white">Short image call to action</p>
    
                </div>
                <!-- /.col-md-4  -->
                </div>
            </div>
            <!--  /.container  -->
    
    
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Category 3
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
    
    
            <div class="container">
                <div class="row">
                <div class="col-md-4">
                    <span class="text-uppercase text-white">Category 3</span>
                    <ul class="nav flex-column">
                    <li class="nav-item">
                    <a class="nav-link active" href="#">Active</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#">Link item</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#">Link item</a>
                    </li>
                </ul>
                </div>
                <!-- /.col-md-4  -->
                <div class="col-md-4">
                    <ul class="nav flex-column">
                    <li class="nav-item">
                    <a class="nav-link active" href="#">Active</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#">Link item</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#">Link item</a>
                    </li>
                </ul>
                </div>
                <!-- /.col-md-4  -->
                <div class="col-md-4">
    
                    <a href="">
                    <img src="https://dummyimage.com/200x100/ccc/000&text=image+link" alt="" class="img-fluid">
                    </a>
                    <p class="text-white">Short image call to action</p>
                    
                </div>
                <!-- /.col-md-4  -->
                </div>
            </div>
            <!--  /.container  -->
    
    
            </div>
        </li>
        </ul>
        <form class="search">
            <div class="search__wrapper">
                <input type="text" name="" placeholder="Search for..." class="search__field">
                <button type="submit" class="fa fa-search search__icon"></button>
            </div>
        </form>
        <!-- <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
        </form> -->
    </div>              
</nav>
</div>