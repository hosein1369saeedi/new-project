<div class="about-us">
    <section class="info-section  py-0">
        <br>
        <div class="titles container">
            <h3 align="center"> About Us </h3>
        </div>
        <br>
        <div class="container">

            <div class="row">
                <div class="col-md-6  content-half ">
                    <br>
                    <p>
                        Parstronic is a heaven for responsible, passionate, self-management and innovative staff. We were established in Tehran in Jul 2015. It is a technology-driven high-intelligent elevator manufacturing company.
                         </p>
                    <p>
                        Our forward-looking technology drives business development, quality is the first to achieve product manufacturing, user experience to promote products and services, to create a perfect and convenient elevator experience. (To create perfect experience in elevator industries)
                    </p>
                    <p>
                        Here, there are top talents from more than 80 people, 60% of whom are R&D personnel (employee or staff).
                        The core team integrates top experts in the elevator and electronic industries to ensure the cross-border thinking and openness of Parstronic"
                    </p>
                    <p>
                        Elevator integrated drive combines elevator and motor control units in a single package. It's more efficient and cost effective.
                    </p>
                    <p>
                        Elevator motion control system (EMCS1000) is a total solution for controlling elevator functionality and safety.
                    </p>
                    <a href="#" class="btn btn-primary btn-capsul px-4 py-2">Explore More</a>
                    <br>
                    <br>
                    <br>



<!--                    <ul class="pl-5">-->
<!---->
<!--                        <li>-->
<!--                            <span class="list-content">-->
<!--                                <strong>Cloud Storage</strong>-->
<!--                                <br>Eiusmod tempor incididunt ut labore et dolore magna aliqua.-->
<!--                            </span>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <span class="list-content">-->
<!--                                <strong>Premium Features</strong>-->
<!--                                <br>Eiusmod tempor incididunt ut labore et dolore magna aliqua.-->
<!--                            </span>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <span class="list-content">-->
<!--                                <strong>Premium Features</strong>-->
<!--                                <br>Eiusmod tempor incididunt ut labore et dolore magna aliqua.-->
<!--                            </span>-->
<!--                        </li>-->
<!--                    </ul>-->
                </div>
                <div class="col-md-6 ">
                    <img src="images/intro-bg-1.jpg" class=" img01" style="position: absolute">
                </div>
            </div>
        </div>
    </section>
</div>