<div class="info-section brands">
    <br>
    <div class="titles container">
        <h3 align="center">Brand</h3>
    </div>
    <br>
    <div class="container">
        <!-- <div class="head-box text-left mb-5"> -->

        <!-- <h6 class="text-underline-primary">This is information panel</h6> -->
        <!-- </div> -->
        <div class="three-panel-block mt-5">
            <div class="row">
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="service-block-overlay text-center mb-5 p-lg-3">
                        <img src="images/b1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="service-block-overlay text-center mb-5 p-lg-3">
                        <img src="images/b2.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="service-block-overlay text-center mb-5 p-lg-3">
                        <img src="images/b3.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="service-block-overlay text-center p-lg-3">
                        <img src="images/b4.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="service-block-overlay text-center p-lg-3">
                        <img src="images/b5.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="service-block-overlay text-center p-lg-3">
                        <img src="images/b6.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>