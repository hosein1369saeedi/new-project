<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top top-nav menu">
    <a class="navbar-brand" href="#">
        <img src="images/logo-parstronic.png" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="dropdown">
                <button class="dropbtn">Company</button>
                <div class="dropdown-content">
                    <a href="#">Milestones</a>
                    <a href="#">Management Team</a>
                    <a href="#">Visions & Values</a>
                </div>
            </li>
            <li class="dropdown">
                <button class="dropbtn">Product</button>
                <div class="dropdown-content">
                    <a href="#">Elevator moution control system</a>
                    <a href="#">Electronic integrated drive</a>
                </div>
            </li>
            <li class="dropdown">
                <button class="dropbtn">Solution</button>
            </li>
            <li class="dropdown">
                <button class="dropbtn">News</button>
            </li>
            <li class="dropdown">
                <button class="dropbtn">Careers</button>

            </li>

            <li class="dropdown">
                <button class="dropbtn">Contact</button>

            </li>
        </ul>
        <div class="social container">
            <ul>
                <li>
                    <div id="myOverlay" class="overlay">
                        <span class="closebtn" onclick="closeSearch()" title="Close Overlay">×</span>
                        <div class="overlay-content">
                            <form action="/action_page.php">
                                <input type="text" placeholder="Search.." name="search">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                    <a  onclick="openSearch()"><i class="fa fa-search"></i></a>
                </li>
                <li><a href=""><span class="flaticon flaticon-twitter-logo"></span></a></li>
                <li><a href=""><span class="flaticon flaticon-facebook-letter-logo"></span></a></li>
                <li><a href=""><span class="flaticon flaticon-linkedin"></span></a></li>
                <li><a href=""><span class="flaticon flaticon-telegram"></span></a></li>
                <li><a href=""><span class="flaticon flaticon-instagram"></span></a></li>
                <li class="flag-right">
                    <a href=""><img src="images/england.png" alt=""></a>
                </li>
                <li class="flag-left">
                    <a href=""><img src="images/iran(1).png" alt=""></a>
                </li>
            </ul>
        </div>
        <!-- <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
        </form> -->
    </div>
</nav>